package model

// TagDetails links the tag with other tags and the tag with article ids
type TagDetails struct {
	Tag           string   `json:"tag"`
	Count         uint     `json:"count"`
	ArticleIDs    []uint64 `json:"articles"`
	RelatedTagIDs []string `json:"related_tags"`
}
