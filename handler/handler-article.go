package handler

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/zenyuk_go/articles-and-tags/model"
)

// TODO: temp. in memory storage
var articlesRepository = make(map[uint64]model.Article)

// GetArticle returns an Article by ID
func GetArticle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	idStr, ok := vars["id"]
	if !ok {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	id, err := strconv.ParseUint(idStr, 0, 32)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	article, ok := articlesRepository[id]
	if !ok {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.Encode(article)
}

// CreateArticle adds a new Article
func CreateArticle(w http.ResponseWriter, r *http.Request) {
	var article model.Article
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&article)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	articlesRepository[article.ID] = article
	w.WriteHeader(http.StatusCreated)
}
